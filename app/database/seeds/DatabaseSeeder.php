<?php

class DatabaseSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		
		$this->call('BookTableSeeder');
		$this->call('MemberTableSeeder');
		$this->call('LoanTableSeeder');
		$this->call('ReviewTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('PreferenceTableSeeder');
	}
}

class BookTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('books')->delete();
		
		DB::table('books')->insert(
				
				array(
						array('id' => 1,'title' => "The Hobbit",'category' => 'Literature','author' => 'J.R.R. Tolkien',
								'isbn' => '8967894236','publisher' => 'Oxford Press','publishDate' => '1917-08-03',
								'status' => 'on loan','created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('id' => 2,'title' => "The Lord Of The Rings",'category' => 'Literature',
								'author' => 'J.R.R. Tolkien','isbn' => '3647596478','publisher' => 'Oxford Press',
								'publishDate' => '1920-06-05','status' => 'on loan','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('id' => 3,'title' => "1984",'category' => 'Literature','author' => 'George Orwell',
								'isbn' => '1596742398','publisher' => 'Penguin Books','publishDate' => '1954-04-25',
								'status' => 'on loan','created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('id' => 4,'title' => "The Wind In The Willows",'category' => 'Childrens',
								'author' => 'Kenneth Grahame','isbn' => '5236978451','publisher' => 'Penguin Books',
								'publishDate' => '1908-08-23','status' => 'available','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('id' => 5,'title' => "The Origin Of The Species",'category' => 'Science',
								'author' => 'Charles Darwin','isbn' => '5678423947','publisher' => 'Oxford Press',
								'publishDate' => '1756-03-15','status' => 'available','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('id' => 6,'title' => "The Hobbit",'category' => 'Literature','author' => 'J.R.R. Tolkien',
								'isbn' => '8967894236','publisher' => 'Oxford Press','publishDate' => '1917-08-03',
								'status' => 'on loan','created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('id' => 7,'title' => "The Lord Of The Rings",'category' => 'Literature',
								'author' => 'J.R.R. Tolkien','isbn' => '3647596478','publisher' => 'Oxford Press',
								'publishDate' => '1920-06-05','status' => 'on loan','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('id' => 8,'title' => "1984",'category' => 'Literature','author' => 'George Orwell',
								'isbn' => '1596742398','publisher' => 'Penguin Books','publishDate' => '1954-04-25',
								'created_at' => '2014-01-01','status' => 'available','updated_at' => '2014-01-01'
						),
						array('id' => 9,'title' => "The Wind In The Willows",'category' => 'Childrens',
								'author' => 'Kenneth Grahame','isbn' => '5236978451','publisher' => 'Penguin Books',
								'publishDate' => '1908-08-23','status' => 'available','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('id' => 10,'title' => "The Origin Of The Species",'category' => 'Science',
								'author' => 'Charles Darwin','isbn' => '5678423947','publisher' => 'Oxford Press',
								'publishDate' => '1756-03-15','status' => 'on loan','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('id' => 11,'title' => "Introductory Macroeconomics",'category' => 'Business',
								'author' => 'Alan Greenspan','isbn' => '2689748569','publisher' => 'Oxford Press',
								'publishDate' => '1992-03-15','status' => 'on loan','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('id' => 12,'title' => "Mahatma Gandhi",'category' => 'Biographies',
								'author' => 'Rajmohan Gandhi','isbn' => '1789647569','publisher' => 'Oxford Press',
								'publishDate' => '1963-03-15','status' => 'available','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						)
				));
	}
}

class MemberTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('members')->delete();
		
		DB::table('members')->insert(
				
				array(
						array('id' => 1,'username' => 'phennessy','password' => 'password','first_name' => 'Patrick',
								'last_name' => 'Hennessy','postal_address' => '4 Main Street, Ardfert, Co. Kerry',
								'member_email' => 'patrick.hennessy@mycit.ie','contact_no' => '0878697428',
								'fine_balance' => 0.0,'member_allowance' => 4,'member_book_quantity' => 1,
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('id' => 2,'username' => 'smeitalova','password' => 'password','first_name' => 'Svetlana',
								'last_name' => 'Meitalova','postal_address' => '6 Main Street, Ennis, Co. Clare',
								'member_email' => 'svetlana.meitalova@mycit.ie','contact_no' => '0873647589',
								'fine_balance' => 0.0,'member_allowance' => 4,'member_book_quantity' => 1,
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('id' => 3,'username' => 'fgalik','password' => 'password','first_name' => 'Frank',
								'last_name' => 'Galik','postal_address' => '8 Main Street, Macroom, Co. Cork',
								'member_email' => 'frank.galik@mycit.ie','contact_no' => '0876475896',
								'fine_balance' => 0.0,'member_allowance' => 4,'member_book_quantity' => 1,
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('id' => 4,'username' => 'mhealy','password' => 'password','first_name' => 'Mike',
								'last_name' => 'Healy','postal_address' => '10 Main Street, Kenmare, Co. Kerry',
								'member_email' => 'michael.healy@mycit.ie','contact_no' => '0875789647',
								'fine_balance' => 0.0,'member_allowance' => 4,'member_book_quantity' => 0,
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('id' => 5,'username' => 'asmoker','password' => 'password','first_name' => 'Aidan',
								'last_name' => 'Smoker','postal_address' => '12 Main Street, Cork City, Co. Cork',
								'member_email' => 'aidan.smoker@mycit.ie','contact_no' => '0877558672',
								'fine_balance' => 0.0,'member_allowance' => 4,'member_book_quantity' => 0,
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						)
				));
	}
}

class ReviewTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('reviews')->delete();
		
		DB::table('reviews')->insert(
				
				array(
						array('member_id' => 1,'book_id' => 1,'review_score' => 5,'comment' => 'Great book. Loved it.',
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('member_id' => 1,'book_id' => 2,'review_score' => 4,'comment' => 'Great book. Loved it.',
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('member_id' => 2,'book_id' => 1,'review_score' => 3,'comment' => 'Good book. Liked it.',
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('member_id' => 1,'book_id' => 1,'review_score' => 4,'comment' => 'Great book. Loved it.',
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('member_id' => 1,'book_id' => 2,'review_score' => 5,'comment' => 'Great book. Loved it.',
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('member_id' => 2,'book_id' => 3,'review_score' => 3,'comment' => 'Good book. Liked it.',
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('member_id' => 1,'book_id' => 4,'review_score' => 5,'comment' => 'Great book. Loved it.',
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('member_id' => 2,'book_id' => 5,'review_score' => 3,'comment' => 'Good book. Liked it.',
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('member_id' => 1,'book_id' => 4,'review_score' => 5,'comment' => 'Great book. Loved it.',
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('member_id' => 2,'book_id' => 5,'review_score' => 3,'comment' => 'Good book. Liked it.',
								'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						)
				));
	}
}

class LoanTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('loans')->delete();
		
		DB::table('loans')->insert(
				
				array(
						array('member_id' => 1,'book_id' => 1,'loan_date' => '2014-03-10','created_at' => '2014-03-10',
								'updated_at' => '2014-03-10'
						),
						array('member_id' => 2,'book_id' => 2,'loan_date' => '2014-03-11','created_at' => '2014-03-11',
								'updated_at' => '2014-03-11'
						),
						array('member_id' => 3,'book_id' => 3,'loan_date' => '2014-03-12','created_at' => '2014-03-12',
								'updated_at' => '2014-03-12'
						),
						array('member_id' => 1,'book_id' => 6,'loan_date' => '2014-03-10','created_at' => '2014-03-10',
								'updated_at' => '2014-03-10'
						),
						array('member_id' => 3,'book_id' => 1,'loan_date' => '2014-03-10','created_at' => '2014-03-10',
								'updated_at' => '2014-03-10'
						),
						array('member_id' => 2,'book_id' => 1,'loan_date' => '2014-03-11','created_at' => '2014-03-11',
								'updated_at' => '2014-03-11'
						),
						array('member_id' => 3,'book_id' => 3,'loan_date' => '2014-03-12','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('member_id' => 4,'book_id' => 2,'loan_date' => '2014-03-12','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('member_id' => 4,'book_id' => 7,'loan_date' => '2014-03-12','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('member_id' => 5,'book_id' => 10,'loan_date' => '2014-03-12','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('member_id' => 4,'book_id' => 11,'loan_date' => '2014-03-12','created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						)
				));
	}
}

class UserTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('users')->delete();
		
		DB::table('users')->insert(
				
				array(
						array('username' => 'jstamper','password' => Hash::make('password'),
								'email' => 'jane.stamper@thislibrary.ie','name' => 'Jane Stamper','member_id' => 0,
								'administrator' => true,'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('username' => 'phennessy','password' => Hash::make('password'),
								'member_email' => 'patrick.hennessy@mycit.ie','name' => 'Patrick Hennessy',
								'administrator' => false,'member_id' => 1,'created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('username' => 'smeitalova','password' => Hash::make('password'),
								'member_email' => 'svetlana.meitalova@mycit.ie','name' => 'Svetlana Meitalova',
								'administrator' => false,'member_id' => 2,'created_at' => '2014-01-01',
								'updated_at' => '2014-01-01'
						),
						array('username' => 'fgalik','password' => Hash::make('password'),
								'member_email' => 'frank.galik@mycit.ie','name' => 'Frank Galik','member_id' => 3,
								'administrator' => false,'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('username' => 'mhealy','password' => Hash::make('password'),
								'member_email' => 'micahel.healy@mycit.ie','name' => 'Mike Healy','member_id' => 4,
								'administrator' => false,'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						),
						array('username' => 'asmoker','password' => Hash::make('password'),
								'member_email' => 'aidan.smoker@mycit.ie','name' => 'Aidan Smoker','member_id' => 5,
								'administrator' => false,'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						)
				));
	}
}

class PreferenceTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('preferences')->delete();
		
		DB::table('preferences')->insert(
				
				array(
						array('initial_book_allowance' => 4,'oneyear_book_allowance' => 6,'loan_period' => 14,
								'fine_amount' => 0.05,'num_highest_rated' => 3,'num_most_borrowed' => 3,
								'num_new_members' => 3,'created_at' => '2014-01-01','updated_at' => '2014-01-01'
						)
				));
	}
}