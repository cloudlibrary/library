<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($t)
		{
        	$t->increments('id');
            $t->string('username', 20);
            $t->string('password', 60);
            $t->string('email', 255);
            $t->string('name', 50);
            $t->integer('member_id');
            $t->boolean('administrator')->default(false);
            $t->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}
}
