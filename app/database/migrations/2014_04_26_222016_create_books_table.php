<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('books', function ($t)
		{
			$t->increments('id');
			$t->string('title', 40);
			$t->enum('category', array('Literature', 'Science', 'Biographies', 'Business', 'Childrens'));
			$t->string('author', 30);
			$t->string('isbn', 16);
			$t->string('publisher', 30);
			$t->date('publishDate');
			$t->enum('status', array('on loan','available','reserved'))->default('available');
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('books');
	}
}
