<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreferencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('preferences', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('initial_book_allowance');
			$table->integer('oneyear_book_allowance');
			$table->integer('loan_period');
			$table->float('fine_amount');
			$table->integer('num_highest_rated');
			$table->integer('num_most_borrowed');
			$table->integer('num_new_members');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('preferences');
	}
}
