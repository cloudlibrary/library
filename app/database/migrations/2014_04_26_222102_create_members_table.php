<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('members', function($table)
		{
			$table->increments('id');
			$table->string('username',30);
			$table->string('password',30);
			$table->string('first_name',30);
			$table->string('last_name',30);
			$table->string('postal_address',80);
			$table->string('member_email',30);
			$table->string('contact_no', 20);
			$table->float('fine_balance')->default(0.0);
			$table->integer('member_allowance')->default(4);
			$table->integer('member_book_quantity')->default(0);
			$table->boolean('approved')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('members');
	}
}
