<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('loans', function($table)
		{
			$table->increments('id');
			$table->integer('member_id');
			$table->integer('book_id');
			$table->date('loan_date');
			$table->date('return_date')->nullable();
			$table->timestamps();
			$table->foreign('member_id')->references('id')->on('members');
			$table->foreign('book_id')->references('id')->on('books');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('loans');
	}
}
