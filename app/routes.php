<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Public Homepage
Route::get('/', 'HomeController@showHome');
Route::get('home', 'HomeController@showHome');

//******* Book route *******
Route::resource('book','BookController');
Route::post('searchbyauthors', 'BookController@findByAuthor');
Route::post('searchbytitle', 'BookController@findByTitle');
Route::get('browse', 'BookController@browse');
Route::post('browsebycategory', 'BookController@browseByCategory');
Route::get('topbooks', 'BookController@topBooks');

// Member routes
Route::resource('member','MemberController');
Route::get('approvemember', 'MemberController@showApproveMember');
Route::post('approvemember', 'MemberController@doApproveMember');
Route::get('latestmembers', 'MemberController@recentlyAdded');

// Loan routes
Route::resource('loan','LoanController');
Route::get('returnbook', 'LoanController@showReturnBook');
Route::post('returnbook', 'LoanController@doReturnBook');

// Review routes
Route::resource('review','ReviewController');

// Admin Preferences
Route::resource('preference','PreferenceController');

//Authentication
Route::get('login', 'SecurityController@showLogin');
Route::post('login', 'SecurityController@doLogin');
Route::get('logout', 'SecurityController@doLogout');

//Contact Us
Route::get('contactus', 'ContactusController@index');
Route::post('contactform', 'ContactusController@postContactform');

// Reservation
Route::resource('reservation','ReservationController');