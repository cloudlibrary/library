<?php
use Illuminate\Support\Facades\Validator;

class Book extends Eloquent
{
	/* protected $table = "books";
	protected $guarded = ["id"];
	protected $softDelete = true; */

	public function reviews()
	{
		return $this->hasMany('Review');
	}

	public static function validate($inputs)
	{
		$rules = array('title' => 'Required',
					   'author' => 'Required',
					   'category' => 'Required',
				       'isbn' => 'Required|Min:9|Max:12');
		
		return Validator::make($inputs, $rules);
	}
}