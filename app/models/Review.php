<?php

class Review extends Eloquent
{

	public function book()
	{
		return $this->belongsTo('Book');
	}
	
	public function member()
	{
		return $this->belongsTo('Member');
	}
	
	public static function validate($inputs){
		$rules = array(
				'review_score' 	=> 'Required',
		);
		return Validator::make($inputs, $rules);
	}
}