<?php

class Loan extends Eloquent
{

	public function member()
	{
		return $this->belongsTo('Member');
	}

	public function book()
	{
		return $this->belongsTo('Book');
	}

	public static function validate($inputs)
	{
		$rules = array('member_id' => 'Required',
					'book_id' => 'Required');
		
		return Validator::make($inputs, $rules);
	}

	public static function validateReturn($inputs)
	{
		$rules = array('book_id' => 'Required');
		
		return Validator::make($inputs, $rules);
	}
}