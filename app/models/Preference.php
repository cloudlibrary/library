<?php

class Preference extends Eloquent
{

	public static function validate($inputs)
	{
		$rules = array('initial_book_allowance' => 'Required',
					   'oneyear_book_allowance' => 'Required',
					   'loan_period' => 'Required',
				       'fine_amount' => 'Required',
				       'num_highest_rated' => 'Required',
				       'num_most_borrowed' => 'Required',
				       'num_new_members' => 'Required');
		
		return Validator::make($inputs, $rules);
	}
}