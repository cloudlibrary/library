<?php

use Illuminate\Support\Facades\Validator;

class Reservation extends Eloquent
{
	public function member()
	{
		return $this->belongsTo('Member');
	}

	public function book()
	{
		return $this->belongsTo('Book');
	}

	public static function validate($inputs)
	{
		$rules = array (
				'book_id' => 'Required',
		);
				
		return Validator::make($inputs, $rules);
	}
}