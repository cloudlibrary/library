<?php
use Illuminate\Support\Facades\Validator;
class Member extends Eloquent {

	public function reviews()
	{
		return $this->hasMany('Review');
	}
	
	public function books()
	{
		return $this->hasMany('Book');
	}
	
	public static function validate($inputs){
		$rules = array(
				'username' 	=> 'Required',
				'password' 	=> 'Required',
				'first_name' 	=> 'Required',
				'last_name' 	=> 'Required',
				'postal_address' 	=> 'Required',
				'member_email' 	=> 'Required',
				'contact_no' 	=> 'Required'
		);
		return Validator::make($inputs, $rules);
	}
}