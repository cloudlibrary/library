<?php

class BrowseTest extends TestCase
{

	/**
	 * A test for the signup page.
	 *
	 * @return void
	 */
	public function testBrowseHasBooks()
	{
		$this->call('GET', 'browse');
		
		$this->assertViewHas('books');
	}
}