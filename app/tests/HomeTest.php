<?php

class HomeTest extends TestCase
{

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testHomeVisible()
	{
		$crawler = $this->client->request('GET', '/home');
		
		$this->assertTrue($this->client->getResponse()->isOk());
	}
}