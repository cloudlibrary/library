@extends('layout')

@section('header')
	Cork Cloud Library - Contact Us
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
<div class="wrapper-header ">
	<h3>Contact Us</h3>
</div>

<div class="wrapper-container container">
	<div class="row">
		<div class="col-md-6">
			<h4>
				<small> Fill up the form</small>
			</h4>
			{{ Form::open(array('url'=>'contactform','class'=>'form-vertical','parsley-validate'=>'','novalidate'=>' '))}}

			<ul class="parsley-error-list">
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>

			<div class="form-group  ">
				<label for="ipt"> Your Name</label>
				{{ Form::text('name', null,array('class'=>'form-control', 'placeholder'=>'','required'=>'Your Name' )) }}
			</div>
			<div class="form-group  ">
				<label for="ipt"> Your Email</label>
				{{ Form::text('sender',null,array('class'=>'form-control', 'placeholder'=>'','required'=>'email' )) }}
			</div>
			<div class="form-group  ">
				<label for="ipt"> Subject </label>
				{{ Form::text('subject',null,array('class'=>'form-control', 'placeholder'=>'Subject','required'=>'true' )) }}
			</div>
			<div class="form-group  ">
				<label for="ipt"> Message </label>
				{{Form::textarea('message',null,array('class'=>'form-control','placeholder'=>'Type your message here', 'required'=>'true' )) }}
			</div>
			<div class="form-group  ">
				<button type="submit" class="btn btn-primary ">Send Form</button>
			</div>
			<input name="redirect" value="contact-us" type="hidden">
			{{Form::close() }}
		</div>
		
		<div class="col-md-6 ">
			<h3>Contact Information</h3>
			<p>Tel: 021-123456 &nbsp; Fax: 123-123457</p>
			<p>Email: corkcloudlibrary@gmail.com</p>
		</div>
	</div>
</div>
@stop
