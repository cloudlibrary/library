<html>
<head>
	<title>County Library System</title>
	<link rel="stylesheet" type="text/css" href="{{{URL::to('/css/main.css')}}}">
	<link rel="stylesheet" type="text/css" href="{{{URL::to('/css/bootstrap.min.css')}}}">
	<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
	<script type='text/javascript' href="{{{URL::to('/menu_jquery.js')}}}"></script>
</head>
<body>
	<div id="main">
		<div id="headerwrap">
			<div id="header">
				<div id="logo">
					<img src="{{{URL::to('/images/book-16-64.png')}}}">
					<h1>@yield('header')</h1>
				</div>
			</div>
			<div id="navigationwrap">
				<div id="navigation">
					@section('navigation')
					<div id='cssmenu'>
						<ul>
							<li><a href="{{{URL::to('/home')}}}"><span>Home</span></a></li>
							<li><a href="{{{URL::to('/topbooks')}}}"><span>Top Books</span></a></li>
							<li><a href="{{{URL::to('/latestmembers')}}}"><span>Newest Members</span></a></li>
							@if(Auth::check())
								@if (Auth::user()->member_id == 0)
									@if (Auth::user()->administrator)
										<li><a href="{{{URL::to('preference/1/edit')}}}"><span>Preferences</span></a></li>
									@endif
								@else
									<li><a href="{{{URL::to('/contactus')}}}"><span>Contact Us</span></a></li>
								@endif
							@else
								<li><a href="{{{URL::to('/member/create')}}}"><span>Register</span></a></li>
							@endif
						</ul>
					</div>
					@show
				</div>
				</div>
				<div id="contentliquid">
					<div id="contentwrap">
						<div id="content">
							<!-- will be used to show any messages -->
							@if (Session::has('message'))
							<div class="alert alert-info">{{ Session::get('message') }}</div>
							@endif @yield('content')
						</div>
					</div>
				</div>
				<div id="leftmenuwrap">
					<div id="leftMenu">
						@section('leftmenu')
							@if(Auth::check())
								@if(Auth::user()->member_id == 0)
									<p><a href="{{{URL::to('/book')}}}">Books</a></p>
									<p><a href="{{{URL::to('/member')}}}">Members</a></p>
									<p><a href="{{{URL::to('/loan/create')}}}">Loan Out Book</a></p>
									<p><a href="{{{URL::to('/returnbook')}}}">Return Book</a></p>
									@if(Auth::user()->administrator)
										<p><a href="{{{URL::to('preference/1/edit')}}}">Administrative Preferences</a></p>
									@endif
								@else
									<p><a href="{{{URL::to('browse')}}}">Browse Books</a></p>
									<p><a href="{{{URL::to('loan')}}}">Loan History</a></p>
									<p><a href="{{{URL::to('/reservation/create')}}}">Reserve Book</a></p>
								@endif
							@else
								<p><a href="{{{URL::to('/browse')}}}">Browse Books</a></p>
								<p><a href="{{{URL::to('/contactus')}}}"><span>Contact Us</span></a></p>
							@endif
						@show
					</div>
				</div>
				<div id="footerwrap">
					<div id="footer">
						@if(Auth::check())
							<a href="{{ URL::to('logout') }}">Logout</a>
						@else
							<a href="{{{URL::to('login')}}}">Login</a>
						@endif
						@yield('footer')
					</div>
				</div>
			</div>
		</div>
</body>
</html>