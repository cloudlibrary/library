@extends('layout')

@section('header')
	Loan Out A Book To A Member
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
	{{Form::open(array('route' => 'loan.store'))}}
		<table>
			<tr>
				<td>Member I.D.:</td>
				<td>{{Form::text('member_id')}}</td>
			</tr>
			<tr>
				<td>Book Barcode:</td>
				<td>{{Form::text('book_id')}}</td>
			</tr>
			<tr>
				<td>{{Form::submit('Loan Book')}}</td>
			</tr>
		</table>
	{{Form::close()}}
	
	@if ($errors->has())
		<ul>
			@foreach ($errors->all() as $error)
  			<li> {{ $error  }} </li>
  			@endforeach
		</ul>
	@endif
@stop
