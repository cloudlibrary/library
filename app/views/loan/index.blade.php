@extends('layout')

@section('header')
	Loan History
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Book I.D.</th>
				<th>Book Title</th>
				<th>Loan Date</th>
				<th>Return Date</th>
			</tr>
		</thead>
		<tbody>
		@for($i = 0; $i < count($loans); $i++)
			<tr>
				<td>{{ $books[$i]->id }} </td>
				<td><a href="{{{URL::to('book')}}}/{{{$books[$i]->id}}}">{{{$books[$i]->title}}}</a></td>
				<td>{{ $loans[$i]->loan_date }} </td>
				@if (isset($loans[$i]->return_date)) <td>{{ $loans[$i]->return_date }} </td>
				@else <td>On loan...</td>
				@endif
			</tr>
		@endfor
		</tbody>
	</table>
@stop