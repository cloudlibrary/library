@extends('layout')

@section('header')
	Accept A Members Returned Book
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
	{{Form::open(array('url' => 'returnbook'))}}
		<table>
			<tr>
				<td>Book Barcode:</td>
				<td>{{Form::text('book_id')}}</td>
			</tr>
			<tr>
				<td>{{Form::submit('Return Book')}}</td>
			</tr>
		</table>
	{{Form::close()}}
	
	@if ($errors->has())
		<ul>
			@foreach ($errors->all() as $error)
  			<li> {{ $error  }} </li>
  			@endforeach
		</ul>
	@endif
@stop
