@extends('layout')

@section('header')
	Cork Cloud Library
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
	@if(Auth::check())
		<h3>Welcome {{Auth::user()->name}}</h3>
	@else
		<h3>Welcome to the Cork City Cloud Library</h3>
	@endif
	Please use the navigation pane at the left to view the cloud library facilities

	<p></p>
	
	<h3>Search</h3>

	{{Form::open(array('url' => 'searchbyauthors'))}}
		{{Form::text('author', null, array('placeholder' => 'Search by Author'))}}
		{{Form::submit('Search By Author')}}
 	{{Form::close()}}

	<p></p>
	
	{{Form::open(array('url' => 'searchbytitle'))}}
		 {{Form::text('title', null, array('placeholder' => 'Search by Title'))}}
		 {{Form::submit('Search By Title')}}
	{{Form::close()}}
	
	<!-- List search errors -->
	@if($errors->has())
		<ul>
			@foreach ($errors->all() as $error)
				<li><p style="color:red;font-size:18px;">{{ $error }}</p></li>
			@endforeach
		</ul>
	@endif 

@stop