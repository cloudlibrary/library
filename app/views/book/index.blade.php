@extends('layout')

@section('header')
		{{trans('books.list_title')}}
@stop

@section('leftmenu')
	@parent
@stop
	
@section('content')
	<div class="container">	
		<nav class="navbar navbar-inverse">
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::to('book') }}">View All Books</a></li>
				<li><a href="{{ URL::to('book/create') }}">Add A Book</a>
			</ul>
		</nav>

	
		@if (count($books)<1)
			<p>{{trans('books.list_numfound')}}</p>
		@elseif (count($books) == 1)
			<p>{{trans('books.list_numfound1')}}</p>
		@else
			<p>{{{count($books)}}} {{trans('books.list_numfound2')}}</p>
		@endif
		
		<h3>All The Books</h3>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<td>Title</td>
 					<td>Category</td>
					<td>Author</td>
					<td>ISBN</td>
					<td>Publisher</td>
					<td>Publication Date</td>
					<td>Status</td>
					<td>Review Score</td>
				</tr>
			</thead>
			<tbody>
				@for ($i = 0; $i < count($books); $i++)
				<tr>
					<td><a href="{{{URL::to('book')}}}/{{{$books[$i]->id}}}">{{{$books[$i]->title}}}</a></td>
					<td>{{{$books[$i] -> category}}}</td>
					<td>{{{$books[$i] -> author}}}</td>
					<td>{{{$books[$i] -> isbn}}}</td>
					<td>{{{$books[$i] -> publisher}}}</td>
					<td>{{{$books[$i] -> publishDate}}}</td>
					<td>{{{$books[$i] -> status}}}</td>
					<td>{{{ isset($avg_reviews[$i]) ? $avg_reviews[$i] : '' }}}</td>
					<td>[<a href="{{{URL::to('book')}}}/{{{$books[$i]->id}}}/edit">Edit</a>]</td>
					<td>{{ Form::open(array('route' => array('book.destroy', $books[$i]->id), 'method' => 'delete')) }}
      			{{Form::submit('Delete')}}
			{{ Form::close() }}</td>
				</tr>
				@endfor
			</tbody>
		</table>
		
	</div>
		
@stop