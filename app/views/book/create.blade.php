@extends('layout')

@section('header')
	Add New Book
@stop

@section('leftmenu')
	@parent
@stop 

@section('content')
<div class="container">
		<nav class="navbar navbar-inverse">
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::to('book') }}">Back to Books</a></li>
			</ul>
		</nav>
		<h3>Add a Book</h3>
	{{Form::open(array('route' => 'book.store'))}}
		<table>
			<tr>
				<td>Title:</td>
				<td>{{Form::text('title')}}</td>
			</tr>
			<tr>
				<td>Category:</td>
				<td>{{Form::select('category', array('Literature', 'Science', 'Biographies', 'Business', 'Childrens'))}}</td>
			</tr>
			<tr>
				<td>Author:</td>
				<td>{{Form::text('author')}}</td>
			</tr>
			<tr>
				<td>ISBN:</td>
				<td>{{Form::text('isbn')}}</td>
			</tr>
			<tr>
				<td>Publisher:</td>
				<td>{{Form::text('publisher')}}</td>
			</tr>
			<tr>
				<td>Publication Date:</td>
				<td>{{Form::text('publishDate')}}</td>
			</tr>
			<tr>
				<td>{{Form::submit('Save')}}</td>
			</tr>
		</table>
	{{Form::close()}} 
	</div>
	
	@if ($errors->has())
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif
@stop
