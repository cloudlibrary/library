@extends('layout')

@section('header')
	Top Books
@stop

@section('leftmenu')
	@parent
@stop
	
@section('content')
	<div class="container">	
		<h2>Highest Reviewed Books</h2>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<td>Title</td>
					<td>Category</td>
					<td>Author</td>
					<td>ISBN</td>
					<td>Publisher</td>
					<td>Publication Date</td>
					<td>Status</td>
					<td>Review Score</td>
				</tr>
			</thead>
			<tbody>
			@for ($i = 0; $i < count($reviewedbooks); $i++)
				<tr>
					<td><a href="{{{URL::to('book')}}}/{{{$reviewedbooks[$i]->id}}}">{{{$reviewedbooks[$i]->title}}}</a></td>
					<td>{{{$reviewedbooks[$i] -> category}}}</td>
					<td>{{{$reviewedbooks[$i] -> author}}}</td>
					<td>{{{$reviewedbooks[$i] -> isbn}}}</td>
					<td>{{{$reviewedbooks[$i] -> publisher}}}</td>
					<td>{{{$reviewedbooks[$i] -> publishDate}}}</td>
					<td>{{{$reviewedbooks[$i] -> status}}}</td>
					<td>{{{ isset($avg_reviews[$i]) ? $avg_reviews[$i] : '' }}}</td>
				</tr>
			@endfor
			</tbody>
		</table>
		<br>
		<h2>Most Popular Books</h2>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<td>Title</td>
					<td>Category</td>
					<td>Author</td>
					<td>ISBN</td>
					<td>Publisher</td>
					<td>Publication Date</td>
					<td>Status</td>
					<td>Loan Quantity</td>
				</tr>
			</thead>
			<tbody>
			@for ($i = 0; $i < count($popularbooks); $i++)
				<tr>
					<td><a href="{{{URL::to('book')}}}/{{{$popularbooks[$i]->id}}}">{{{$popularbooks[$i]->title}}}</a></td>
					<td>{{{$popularbooks[$i] -> category}}}</td>
					<td>{{{$popularbooks[$i] -> author}}}</td>
					<td>{{{$popularbooks[$i] -> isbn}}}</td>
					<td>{{{$popularbooks[$i] -> publisher}}}</td>
					<td>{{{$popularbooks[$i] -> publishDate}}}</td>
					<td>{{{$popularbooks[$i] -> status}}}</td>
					<td>{{{ isset($loan_quantity[$i]) ? $loan_quantity[$i] : '' }}}</td>
				</tr>
			@endfor
			</tbody>
		</table>
	</div>
@stop