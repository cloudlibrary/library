@extends('layout')

@section('header')
	Browse Books
@stop

@section('leftmenu')
	@parent
@stop
	
@section('content')
	<div class="container">
	@if($showcategory)
		{{Form::open(array('url' => 'browsebycategory'))}}
		{{Form::select('category', array('Literature', 'Science', 'Biographies', 'Business', 'Childrens'))}}</td>
		{{Form::submit('Narrow Category')}}
		{{Form::close()}}
		<h2>{{{ isset($category) ? $category : 'All Books' }}}</h2>
	@else
		<h2>{{{ isset($category) ? $category : 'Books Found' }}}</h2>
	@endif	
		
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<td>Book Barcode</td>
					<td>Title</td>
					<td>Category</td>
					<td>Author</td>
					<td>ISBN</td>
					<td>Publisher</td>
					<td>Publication Date</td>
					<td>Status</td>
					<td>Review Score</td>
				</tr>
			</thead>
			<tbody>
			@for ($i = 0; $i < count($books); $i++)
				<tr>
					<td>{{{$books[$i] -> id}}}</td>
					<td><a href="{{{URL::to('book')}}}/{{{$books[$i]->id}}}">{{{$books[$i]->title}}}</a></td>
					<td>{{{$books[$i] -> category}}}</td>
					<td>{{{$books[$i] -> author}}}</td>
					<td>{{{$books[$i] -> isbn}}}</td>
					<td>{{{$books[$i] -> publisher}}}</td>
					<td>{{{$books[$i] -> publishDate}}}</td>
					<td>{{{$books[$i] -> status}}}</td>
					<td>{{{ isset($avg_reviews[$i]) ? $avg_reviews[$i] : '' }}}</td>
				</tr>
			@endfor
			</tbody>
		</table>
	</div>
@stop