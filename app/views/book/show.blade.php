@extends('layout')

@section('header')
	{{{$book->title}}}
 	@if(Auth::check() && Auth::user()->member_id == 0)
		[<a href="{{{URL::to('book')}}}/{{{$book->id}}}/edit">Edit</a>]
	@endif
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
<div class="container">
	@if(Auth::check() && Auth::user()->member_id == 0)
		<nav class="navbar navbar-inverse">
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::to('book') }}">Back to Books</a></li>
			</ul>
		</nav>
		@endif
	<table class="table table-bordered">
		<tr>
			<td>Title:</td>
			<td>{{{$book->title}}}</td>
		</tr>
		<tr>
			<td>Category:</td>
			<td>{{{$book->category}}}</td>
		</tr>
		<tr>
			<td>ISBN:</td>
			<td>{{{$book->isbn}}}</td>
		</tr>
		<tr>
			<td>Published:</td>
			<td>{{{$book->publishDate}}}</td>
		</tr>
		<tr>
			<td>Author: </td>
			<td>{{{$book->author}}}</td>
		</tr>
		<tr>
			<td>Publisher:</td>
			<td>{{{$book->publisher}}}</td>
		</tr>
		<tr>
			<td>Status:</td>
			<td>{{{$book->status}}}</td>
		</tr>
	</table>
	</div>
	
	<p>Reviews of this book:</p>
	@if (count($reviews)<1)
		<p>No reviews yet...<p>
	@else
		<ul>
			@foreach($reviews as $review)
				<li>Score: {{{$review->review_score}}} Comment: {{{$review->comment}}}</li>
			@endforeach
		</ul>
	@endif

	@if(Auth::check())
		@if(Auth::user()->member_id == 0)
			{{Form::open(array('route' => array('book.destroy', $book->id), 'method' => 'delete'))}}
			{{Form::submit('Delete')}}
			{{Form::close()}}
		@else
			{{Form::open(array('route' => 'review.store'))}}
				<table>
					<tr>
						<td>Review Score:</td>
						<td>{{Form::selectRange('review_score', 1, 5)}}</td>
					</tr>
					<tr>
						<td>Optional Comment:</td>
						<td>{{Form::text('comment')}}</td>
					</tr>
					<tr>
						<td>{{Form::hidden('book_id', $book->id)}}</td>
					</tr>
					<tr>
						<td>{{Form::submit('Submit Review')}}</td>
					</tr>
				</table>
			{{Form::close()}}
			@if ($errors->has())
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
				</ul>
			@endif
		@endif
	@endif
@stop
