@extends('layout')

@section('header')
	Book Update - {{{$book->title}}}
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
	<div class="container">
		<nav class="navbar navbar-inverse">
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::to('book') }}">Back to Books</a></li>
			</ul>
		</nav>
		<h3>Update a Book</h3>
	{{Form::model($book, array('route' => array('book.update', $book->id), 'method' => 'put'))}}
		<table>
			<tr>
				<td>{{Form::label('id', $book->id)}}</td>
			</tr>
			<tr>
				<td>Title:</td>
				<td>{{Form::text('title')}}</td>
			</tr>
			<tr>
				<td>Category:</td>
				<td>{{Form::select('category', array('Literature', 'Science', 'Biographies', 'Business', 'Childrens'))}}</td>
			</tr>
			<tr>
				<td>Author:</td>
				<td>{{Form::text('author')}}</td>
			</tr>
		 	<tr>
		 		<td>ISBN:</td>
		 		<td>{{Form::text('isbn')}}</td>
		 	</tr>
		 	<tr>
		 		<td>Publisher:</td>
		 		<td>{{Form::text('publisher')}}</td>
		 	</tr>
		 	<tr>
		 		<td>Publication Date:</td>
		 		<td>{{Form::text('publishDate')}}</td>
		 	</tr>
		 	<tr>
		 		<td>Status:</td>
		 		<td>{{Form::label('status', $book->status)}}</td>
		 	</tr>
		 	<tr>
		 		<td>{{Form::submit('Update')}}</td>
		 	</tr>
 		</table>
	{{Form::close()}}
 	</div>

	@if ($errors->has())
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif
@stop
