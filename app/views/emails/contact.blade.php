<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
  	<h2>Message Sent Via Cloud Library Contact Form</h2><br>
    <h2>The details are as follows:</h2><br>
    <div>Sender Name: {{$contactName}}</div>
    <div>Sender Email: {{$contactSender}}</div>
    <div>Subject: {{$contactSubject}}</div>
    <div>Message: {{$contactMessage}}</div>
  </body>
</html>