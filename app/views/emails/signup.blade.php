<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Thank you for signing up to the Cork Cloud Library {{$name}}.</h2>
    <h3>Please attend the county library with details of proof of identity
    	and proof of address to complete Library membership.</h3><br>
    <div>When you are approved your login details will be:</div>
    <div>Username: {{$username}}</div>
    <div>Password: {{$password}}</div>
  </body>
</html>