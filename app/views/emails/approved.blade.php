<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
  	<h2>Congratulations {{$name}}.</h2>
    <h2>You have been approved as a member of the Cork Cloud Library</h2><br>
    <div>Welcome to the library. Your site login details are:</div>
    <div>Username:{{$username}}</div>
    <div>Password:{{$password}}</div>
  </body>
</html>