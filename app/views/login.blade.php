@extends('layout')

@section('header')
	Please Login
@stop

@section('content')
	{{Form::open(array('url' => 'login'))}}
		<table>
			<tr>
				<td>{{Form::label('username', 'Username')}}</td>
				<td>{{Form::text('username', '')}}</td>
			</tr>
			<tr>
				<td>{{Form::label('password', 'Password')}}</td>
				<td>{{Form::password('password')}}</td>
			</tr>
		</table>
		<p>{{Form::submit('Login')}}</p>
	{{Form::close()}}
	
	<!-- List any login errors -->
	@if($errors->has())
		<ul>
			@foreach ($errors->all() as $error)
				<li><p style="color:red;font-size:18px;">{{ $error }}</p></li>
			@endforeach
		</ul>
	@endif
@stop