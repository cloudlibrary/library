@extends('layout') 

@section('header') 	
	Administrative Preferences
@stop

@section('leftmenu')
	@parent
@stop

@section('content') 
	<h3>Preferences</h3>
	{{Form::model($preference, array('route' => array('preference.update', $preference->id), 'method' => 'put'))}}
		<table class="table table-hover">
			<tr>
				<td>Initial Book Allowance:</td>
				<td>{{Form::text('initial_book_allowance', null, array('placeholder' => $preference->initial_book_allowance))}}</td>
			</tr>
			<tr>
				<td>One Year Book Allowance:</td>
				<td>{{Form::text('oneyear_book_allowance', null, array('placeholder' => $preference->oneyear_book_allowance))}}</td>
			</tr>
			<tr>
				<td>Loan Period:</td>
				<td>{{Form::text('loan_period', null, array('placeholder' => $preference->loan_period))}}</td>
			</tr>
			<tr>
				<td>Fine Amount:</td>
				<td>{{Form::text('fine_amount', null, array('placeholder' => $preference->fine_amount))}}</td>
			</tr>
			<tr>
				<td>Number of Highest Rated Books on Homepage:</td>
				<td>{{Form::text('num_highest_rated', null, array('placeholder' => $preference->num_highest_rated))}}</td>
			</tr>
			<tr>
				<td>Number of Most Popular Books on Homepage:</td>
				<td>{{Form::text('num_most_borrowed', null, array('placeholder' => $preference->num_most_borrowed))}}</td>
			</tr>
			<tr>
				<td>Number of New Members on Homepage:</td>
				<td>{{Form::text('num_new_members', null, array('placeholder' => $preference->num_new_members))}}</td>
			</tr>
			<tr>
				<td>{{Form::submit('Update')}}</td>
			</tr>
		</table>
	{{Form::close()}}
	<p>N.B.: When you update the system preferences, all members that have been signed up</p>
	<p>      for over a year, and have no outstanding fines, will have their book allowance</p>
	<p>      increased to the new one year allowance.</p>
	
	@if ($errors->has())
		<ul>
			@foreach ($errors->all() as $error)
  			<li> {{ $error  }} </li>
  			@endforeach
		</ul>
	@endif
@stop 



