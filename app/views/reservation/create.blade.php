@extends('layout')

@section('header')
	Make a Book Reservation
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
	<h3>Please enter reservation details</h3>
	{{Form::open(array('route' => 'reservation.store'))}}
		<table>
			<tr>
				<td>Book Barcode:</td>
				<td>{{Form::text('book_id')}}</td>
			</tr>
			<tr>
				<td>{{Form::submit('Reserve')}}</td>
			</tr>
		</table>
	{{Form::close()}}
	
	@if ($errors->has())
		<ul>
			@foreach ($errors->all() as $error)
  			<li> {{ $error  }} </li>
  			@endforeach
		</ul>
	@endif
@stop

