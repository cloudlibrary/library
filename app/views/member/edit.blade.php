@extends('layout') 

@section('header') 	
	Edit {{{$member->title}}} 
@stop

@section('leftmenu') 

@section('content') 
	<div class="container">
		<nav class="navbar navbar-inverse">
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::to('member') }}">Back to Member List</a></li>
			</ul>
		</nav>
		<h3>Update a Member</h3>
	{{Form::model($member, array('route' => array('member.update', $member->id), 'method' => 'put'))}}
		<table>
			<tr>
				<td>Member ID:</td>
				<td>{{Form::label('id', $member->id)}}</td>
			</tr>
			<tr>
				<td>Username:</td>
				<td>{{Form::text('username')}}</td>
			</tr>
			<tr>
				<td>Password:</td>
				<td>{{Form::text('password')}}</td>
			</tr>
			<tr>
				<td>First Name:</td>
				<td>{{Form::text('first_name')}}</td>
			</tr>
			<tr>
				<td>Last Name:</td>
				<td>{{Form::text('last_name')}}</td>
			</tr>
			<tr>
				<td>Postal Address:</td>
				<td>{{Form::text('postal_address')}}</td>
			</tr>
			<tr>
				<td>Email:</td>
				<td>{{Form::text('member_email')}}</td>
			</tr>
			<tr>
				<td>Contact No:</td>
				<td>{{Form::input('number','contact_no')}}</td>
			</tr>
			<tr>
				<td>Book Allowance:</td>
				<td>{{Form::text('member_allowance')}}</td>
			</tr>
			<tr>
				<td>Fine Balance:</td>
				<td>{{Form::text('fine_balance')}}</td>
			</tr>
			<tr>
				<td>{{Form::submit('Update')}}</td>
			</tr>
		</table>
		</div>
	{{Form::close()}}
	
	@if ($errors->has())
		<ul>
			@foreach ($errors->all() as $error)
  			<li> {{ $error  }} </li>
  			@endforeach
		</ul>
	@endif
@stop 



