@extends('layout')

@section('header')
	Newest Members
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
	<div class="container">
		<h3>New Members</h3>
	
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<td>Member Id</td>
					<td>First Name</td>
					<td>Last Name</td>
					<td>Status</td>
				</tr>
			</thead>
			<tbody>
				@for ($i = 0; $i < count($members); $i++)
				<tr>
					<td>{{{$members[$i] -> id}}}</td>
					<td>{{{$members[$i] -> first_name}}}</td>
					<td>{{{$members[$i] -> last_name}}}</td>
					<td>@if($members[$i] -> approved)
						<p>Approved<p>
						@else
						<p>Unapproved<p>
						@endif</td>
				</tr>
				@endfor
			</tbody>
		</table>
	</div>
@stop