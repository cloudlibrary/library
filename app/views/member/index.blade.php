@extends('layout')

@section('header')
	Library Member List
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
	<div class="container">
		<nav class="navbar navbar-inverse">
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::to('approvemember') }}">View Unapproved Members</a></li>
			</ul>
		</nav>	

		@if (count($members)<1)
			<p> No Members found!</p>
		@elseif (count($members) == 1)
			<p> Only a single member found!</p>
	 	@else
			<p>Number of current members: {{{count($members)}}}</p>
		@endif
		
		<h3>Member List</h3>
	
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<td>Member Id</td>
					<td>First Name</td>
					<td>Last Name</td>
					<td>Postal Address</td>
					<td>Member Email</td>
					<td>Contact No</td>
					<td>Books On Loan</td>
					<td>Fine Balance</td>
					<td>Status Approved</td>
				</tr>
			</thead>
			<tbody>
				@for ($i = 0; $i < count($members); $i++)
				<tr>
					<td>{{{$members[$i] -> id}}}</td>
					<td>{{{$members[$i] -> first_name}}}</td>
					<td>{{{$members[$i] -> last_name}}}</td>
					<td>{{{$members[$i] -> postal_address}}}</td>
					<td>{{{$members[$i] -> member_email}}}</td>
					<td>{{{$members[$i] -> contact_no}}}</td>
					<td>{{{$members[$i] -> member_book_quantity}}}</td>
					<td>{{{$members[$i] -> fine_balance}}}</td>
					<td>@if($members[$i] -> approved)
						<p>Approved<p>
						@else
						<p>Unapproved<p>
						@endif</td>
					<td><a href="{{{URL::to('member')}}}/{{{$members[$i]->id}}}/edit">Edit</a></td>
				</tr>
				@endfor
			</tbody>
		</table>
	</div>
@stop