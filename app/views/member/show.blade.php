@extends('layout')

@section('header')
	{{{$member->last_name}}}
	[<a href="{{{URL::to('member')}}}/{{{$member->id}}}/edit">Edit</a>]
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
<table>
	<tr>
		<td>Last Name:</td>
		<td>{{{$member->last_name}}}</td>
	</tr>
	<tr>
		<td>First Name:</td>
		<td>{{{$member->first_name}}}</td>
	</tr>
	<tr>
		<td>Postal Address:</td>
		<td>{{{$member->postal_address}}}</td>
	</tr>
	<tr>
		<td>Member Email:</td>
		<td>{{{$member->member_email}}}</td>
	</tr>
</table>
@stop

