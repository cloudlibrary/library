@extends('layout')
 
@section('header')
	New Member Registration
@stop

@section('leftmenu')
	@parent
@stop

@section('content')
	<h3>Please enter your personal details</h3>
	
	{{Form::open(array('route' => 'member.store'))}}
		<table>
			<tr>
				<td>Username:</td>
				<td>{{Form::text('username')}}</td>
			</tr>
			<tr>
				<td>Password:</td>
				<td>{{Form::text('password')}}</td>
			</tr>
			<tr>
				<td>First Name:</td>
				<td>{{Form::text('first_name')}}</td>
			</tr>
			<tr>
				<td>Last Name:</td>
				<td>{{Form::text('last_name')}}</td>
			</tr>
			<tr>
				<td>Postal Address:</td>
				<td>{{Form::text('postal_address')}}</td>
			</tr>
			<tr>
				<td>Email:</td>
				<td>{{Form::text('member_email')}}</td>
			</tr>
			<tr>
				<td>Contact No:</td>
				<td>{{Form::input('number','contact_no')}}</td>
			</tr>
			<tr>
				<td>{{Form::submit('Save')}}</td>
			</tr>
		</table>
	{{Form::close()}}
	
	@if ($errors->has())
		<ul>
			@foreach ($errors->all() as $error)
  			<li> {{ $error  }} </li>
  			@endforeach
		</ul>
	@endif
@stop
