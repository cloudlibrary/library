@extends('layout')

@section('header')
	Unapproved Members
@stop

@section('leftmenu')
	@parent
@stop


	
@section('content')
	<div class="container">
		<nav class="navbar navbar-inverse">
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::to('member') }}">Back to Members</a></li>
			</ul>
		</nav>	

		@if (count($members)<1)
			<h2> No Members found!</h2>
	 	@else
			<h3>List Of Unapproved Members</h3>
	
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<td>Member Id</td>
						<td>First Name</td>
						<td>Last Name</td>
						<td>Postal Address</td>
						<td>Member Email</td>
						<td>Contact No</td>
						<td>Approve</td>
					</tr>
				</thead>
				<tbody>
					@for ($i = 0; $i < count($members); $i++)
					<tr>
						<td>{{{$members[$i] -> id}}}</td>
						<td>{{{$members[$i] -> first_name}}}</td>
						<td>{{{$members[$i] -> last_name}}}</td>
						<td>{{{$members[$i] -> postal_address}}}</td>
						<td>{{{$members[$i] -> member_email}}}</td>
						<td>{{{$members[$i] -> contact_no}}}</td>
						<td>{{Form::open(array('url' => 'approvemember'))}}
							{{Form::hidden('member_id', $members[$i]->id)}}
							{{Form::submit('Approve Member')}}
							{{Form::close()}}</td>
					</tr>
					@endfor
				</tbody>
			</table>
		@endif	
	</div>
@stop