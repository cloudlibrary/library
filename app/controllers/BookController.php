<?php
use library\service\BookService;

class BookController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$message = Session::get('message', '');
		$books = Book::all();
		$avg_reviews = array();
		for($i = 0; $i < count($books); $i++)
		{
			// Get average review score of book
			$avg_reviews[$i] = DB::table('reviews')->where('book_id', $books[$i]->id)->avg('review_score');
		}
		return View::make('book.index')->with('books', $books)->with('avg_reviews', $avg_reviews)->with('message', 
				$message);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('book.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::all();
		$validator = Book::validate($inputs);
		
		$catagories = array('Literature','Science','Biographies','Business','Childrens'
		);
		
		if ($validator->passes())
		{
			$book = new Book();
			
			$book->title = $inputs['title'];
			$book->category = $catagories[$inputs['category']];
			$book->author = $inputs['author'];
			$book->isbn = $inputs['isbn'];
			$book->publisher = $inputs['publisher'];
			$book->publishDate = $inputs['publishDate'];
			$book->save();
			
			return Redirect::route('book.index')->with('message', 'New Book has been added successfuly!');
		}
		else
		{
			return Redirect::route('book.create')->withErrors($validator);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function show($id)
	{
		$book = Book::find($id);
		$reviews = $book->reviews;
		$message = Session::get('message', '');
		
		return View::make('book.show')->with('book', $book)->with('reviews', $reviews)->with('message', $message);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function edit($id)
	{
		$book = Book::find($id);
		return View::make('book.edit')->with('book', $book);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function update($id)
	{
		$inputs = Input::all();
		$validator = Book::validate($inputs);
		
		$catagories = array('Literature','Science','Biographies','Business','Childrens'
		);
		
		if ($validator->passes())
		{
			$book = Book::find($id);
			
			$book->title = $inputs['title'];
			$book->category = $catagories[$inputs['category']];
			$book->author = $inputs['author'];
			$book->isbn = $inputs['isbn'];
			$book->publisher = $inputs['publisher'];
			$book->publishDate = $inputs['publishDate'];
			$book->update();
			
			return Redirect::route('book.show', $id)->with('message', 'Book updated.');
		}
		else
		{
			return Redirect::route('book.edit', $id)->withErrors($validator);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function destroy($id)
	{
		$book = Book::find($id);
		$book->delete();
		
		return Redirect::route('book.index')->with('message', 'Book successfully deleted!');
	}

	/**
	 * Handle search by author
	 */
	public function findByAuthor()
	{
		$message = Session::get('message', '');
		$author = Input::get('author');
		$books = Book::where('author', 'LIKE', "%" . $author . "%")->get(); // Get books with author like the input
		$avg_reviews = array();
		for($i = 0; $i < count($books); $i++)
		{
			$avg_reviews[$i] = DB::table('reviews')->where('book_id', $books[$i]->id)->avg('review_score');
		}
		return View::make('book.browse')->with('books', $books)->with('avg_reviews', $avg_reviews)->with('message', 
				$message)->with('showcategory', false);
	}

	/**
	 * Handle search by title
	 */
	public function findByTitle()
	{
		$message = Session::get('message', '');
		$title = Input::get('title');
		$books = Book::where('title', 'LIKE', "%" . $title . "%")->get(); // Get books with title like input
		$avg_reviews = array();
		for($i = 0; $i < count($books); $i++)
		{
			$avg_reviews[$i] = DB::table('reviews')->where('book_id', $books[$i]->id)->avg('review_score');
		}
		return View::make('book.browse')->with('books', $books)->with('avg_reviews', $avg_reviews)->with('message', 
				$message)->with('showcategory', false);
	}

	/**
	 * Handle browse page GET
	 */
	public function browse()
	{
		$message = Session::get('message', '');
		$books = Book::all();
		$avg_reviews = array();
		for($i = 0; $i < count($books); $i++)
		{
			$avg_reviews[$i] = DB::table('reviews')->where('book_id', $books[$i]->id)->avg('review_score');
		}
		return View::make('book.browse')->with('books', $books)->with('avg_reviews', $avg_reviews)->with('message', 
				$message)->with('showcategory', true);
	}

	/**
	 * Handle browse page narrowing of category
	 */
	public function browseByCategory()
	{
		$catagories = array('Literature','Science','Biographies','Business','Childrens'
		);
		$message = Session::get('message', '');
		$category = $catagories[Input::get('category')];
		$books = Book::where('category', $category)->get(); // Only books of the input 'category'
		$avg_reviews = array();
		for($i = 0; $i < count($books); $i++)
		{
			$avg_reviews[$i] = DB::table('reviews')->where('book_id', $books[$i]->id)->avg('review_score');
		}
		return View::make('book.browse')->with('books', $books)->with('avg_reviews', $avg_reviews)->with('message', 
				$message)->with('category', $category)->with('showcategory', true);
	}

	/**
	 * Handle GET of top books page
	 */
	public function topBooks()
	{
		// Get the reviews grouped by book, ordered by score
		$reviews = Review::groupBy('book_id')->orderBy(DB::raw('AVG(review_score)'), 'DESC')->get();
		$reviewNumber = Preference::find(1)->num_highest_rated; // Limit to preference value
		$reviewedbooks = array();
		for($i = 0; $i < $reviewNumber; $i++)
		{
			// Get the 'num_highest_rated' number of books associated with the reviews
			$reviewBookId = $reviews[$i]->book_id;
			$reviewedbooks[$i] = Book::find($reviewBookId);
		}
		$avg_reviews = array();
		for($i = 0; $i < count($reviewedbooks); $i++)
		{
			// Also get their review averages
			$avg_reviews[$i] = DB::table('reviews')->where('book_id', $reviewedbooks[$i]->id)->avg('review_score');
		}
		
		// Get the loans grouped by book, ordered by count
		$loans = Loan::groupBy('book_id')->orderBy(DB::raw('COUNT(*)'), 'DESC')->get();
		$topNumber = Preference::find(1)->num_most_borrowed;
		$popularbooks = array();
		for($i = 0; $i < $topNumber; $i++)
		{
			// Get the books associated with each loan
			$topBookId = $loans[$i]->book_id;
			$popularbooks[$i] = Book::find($topBookId);
		}
		$loan_quantity = array();
		for($i = 0; $i < count($popularbooks); $i++)
		{
			// Get the number of times loaned
			$loan_quantity[$i] = Loan::where('book_id', $popularbooks[$i]->id)->count();
		}
		return View::make('book.top')->with('popularbooks', $popularbooks)->with('reviewedbooks', $reviewedbooks)->with(
				'avg_reviews', $avg_reviews)->with('loan_quantity', $loan_quantity);
	}
}
