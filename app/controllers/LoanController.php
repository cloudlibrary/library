<?php

class LoanController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$message = Session::get('message', '');
		$memberId = Auth::user()->member_id;
		$loans = Loan::where('member_id', $memberId)->get(); // Only loans of a particular member
		$books = array();
		for($i = 0; $i < count($loans); $i++)
		{
			$books[$i] = Book::find($loans[$i]->book_id);
		}
		return View::make('loan.index')->with('loans', $loans)->with('books', $books)->with('message', $message);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('loan.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::all();
		$validator = Loan::validate($inputs);
		
		if ($validator->passes())
		{
			$loans = Loan::where('book_id', $inputs['book_id'])->whereNull('return_date')->get();
			if (count($loans) > 0)
			{
				// Check that the Librarian is not entering the barcode of a book on loan
				return Redirect::action('LoanController@create')->with('message', 'That book is already on loan.');
			}
			
			// Check if book is reserved
			$memberId = $inputs['member_id'];
			$existingReservations = Reservation::where('book_id', $inputs['book_id'])->get();
			if (count($existingReservations) > 0)
			{
				$existingReservation = $existingReservations[0];
				
				// If reserved, is it past 7 days
				$resTime = strtotime($existingReservation->created_at);
				$rawDays = (time() - $resTime) / (60 * 60 * 24);
				$days = round($rawDays);
				if ($days < 8)
				{
					if ($existingReservation->member_id == $memberId)
					{
						// If it's reserved but it's the right member picking it up
						// get rid of reservation
						$existingReservation->delete();
					}
					else
					{
						// Else, someone trying to loan a book reserved for another member
						return Redirect::action('LoanController@create')->with('message', 
								'Sorry. This book is reserved.');
					}
				}
			}
			
			// Check the member is within their allowance
			$member = Member::find($memberId);
			if ($member->member_book_quantity >= $member->member_allowance)
			{
				return Redirect::action('LoanController@create')->with('message', 
						'Member has reached their book allowance limit.');
			}
			else
			{
				// Make the loan
				$loan = new Loan();
				$loan->member_id = $memberId;
				$loan->book_id = $inputs['book_id'];
				$loan->loan_date = date('Y-m-d');
				$loan->save();
				
				// Update members quantity of books
				$member->member_book_quantity = $member->member_book_quantity + 1;
				$member->update();
				
				// Update book
				$book = Book::find($inputs['book_id']);
				$book->status = 'on loan';
				$book->update();
				return Redirect::action('LoanController@create')->with('message', 'Book loaned out successfully.');
			}
		}
		else
		{
			return Redirect::route('loan.create')->withErrors($validator);
		}
	}

	public function showReturnBook()
	{
		return View::make('loan.return');
	}

	public function doReturnBook()
	{
		$inputs = Input::all();
		$validator = Loan::validateReturn($inputs);
		
		if ($validator->passes())
		{
			$book_id = $inputs['book_id'];
			$loans = Loan::where('book_id', $book_id)->whereNull('return_date')->get();
			if (count($loans) > 0)
			{
				// Note book return date
				$loan = $loans[0];
				$loan->return_date = date('Y-m-d');
				$loan->update();
				
				// Decrement members book quantity
				$member_id = $loan->member_id;
				$member = Member::find($member_id);
				$member->member_book_quantity = $member->member_book_quantity - 1;
				
				// Mark book available
				$book = Book::find($inputs['book_id']);
				$book->status = 'available';
				$book->update();
				
				// Calculate if book is late
				$loanTime = strtotime($loan->loan_date);
				$rawDays = (time() - $loanTime) / (60 * 60 * 24);
				$days = round($rawDays);
				if ($days > 14)
				{
					// Get the system preferences
					$preferences = Preference::find(1);
					$fine_amount = $preferences->fine_amount;
					$loan_period = $preferences->loan_period;
					
					// How many days over
					$overdays = $days - $loan_period;
					$fine = $overdays * $fine_amount;
					
					// Fine the member
					$member->fine_balance = $member->fine_balance + $fine;
					$member->update();
					return Redirect::action('LoanController@showReturnBook')->with('message', 
							'Book was overdue by ' . $overdays . " days. Member " . $member->first_name . " " .
									 $member->last_name . " has been fined " . $fine . " euros.");
				}
				else
				{
					// Book not overdue
					$member->update();
					return Redirect::action('LoanController@showReturnBook')->with('message', 
							'Book returned successfully.');
				}
			}
			else
			{
				// Book with I.D. entered is not on loan
				return Redirect::action('LoanController@showReturnBook')->with('message', 
						'No book with that I.D. on loan.');
			}
		}
		else
		{
			return Redirect::to('returnbook')->withErrors($validator);
		}
	}
}