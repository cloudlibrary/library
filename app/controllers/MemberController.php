<?php

class MemberController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$message = Session::get('message', '');
		$members = Member::all();
		return View::make('member.index')->with('members', $members)->with('message', $message);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// Used for member sign up
		return View::make('member.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::all();
		$validator = Member::validate($inputs);
		
		if ($validator->passes())
		{
			$member = new Member();
			$allowance = Preference::find(1)->initial_book_allowance;
			
			// Store the member
			$member->username = $inputs['username'];
			$member->password = $inputs['password'];
			$member->first_name = $inputs['first_name'];
			$member->last_name = $inputs['last_name'];
			$member->postal_address = $inputs['postal_address'];
			$member->member_email = $inputs['member_email'];
			$member->contact_no = $inputs['contact_no'];
			$member->member_allowance = $allowance;
			$member->save();
			
			$members = Member::orderBy('id', 'DESC')->get();
			$newMember = $members[0];
			
			// Create an authorisation level
			$user = new User();
			$user->username = $inputs['username'];
			$user->password = Hash::make($inputs['password']);
			$user->email = $inputs['member_email'];
			$user->name = $inputs['first_name'] . " " . $inputs['last_name'];
			$user->member_id = $newMember->id;
			$user->save();
			
			// Send sign up welcome email
			$this->sendSignupEmail($member);
			return Redirect::route('member.create')->with('message', 
					'New Member registration completed. Please attend the county library with details
					of proof of identity and proof of address to complete Library membership');
		}
		else
		{
			return Redirect::route('member.create')->withErrors($validator);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function show($id)
	{
		$member = Member::find($id);
		$message = Session::get('message', '');
		
		return View::make('member.show')->with('member', $member)->with('message', $message);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function edit($id)
	{
		$member = Member::find($id);
		return View::make('member.edit')->with('member', $member);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function update($id)
	{
		$inputs = Input::all();
		$validator = Member::validate($inputs);
		
		if ($validator->passes())
		{
			$member = Member::find($id);
			
			$member->username = $inputs['username'];
			$member->password = $inputs['password'];
			$member->first_name = $inputs['first_name'];
			$member->last_name = $inputs['last_name'];
			$member->postal_address = $inputs['postal_address'];
			$member->member_email = $inputs['member_email'];
			$member->contact_no = $inputs['contact_no'];
			$member->member_allowance = $inputs['member_allowance'];
			$member->fine_balance = $inputs['fine_balance'];
			$member->update();
			
			$users = User::where('member_id', $id)->get();
			$user = $users[0];
			
			$user->username = $inputs['username'];
			$user->password = Hash::make($inputs['password']);
			$user->email = $inputs['member_email'];
			$user->name = $inputs['first_name'] . " " . $inputs['last_name'];
			$user->update();
			return Redirect::route('member.edit', $id)->with('message', 'Member updated.');
		}
		else
		{
			return Redirect::route('member.edit', $id)->withErrors($validator);
		}
	}

	/**
	 * Show list of unapproved members
	 */
	public function showApproveMember()
	{
		$message = Session::get('message', '');
		$members = Member::where('approved', false)->get();
		return View::make('member.approve')->with('members', $members)->with('message', $message);
	}

	/**
	 * Handle a click of a member approval button
	 */
	public function doApproveMember()
	{
		$member_id = Input::get('member_id');
		$member = Member::find($member_id);
		$member->approved = true;
		$member->update();
		
		// Send approval email
		$this->sendApprovalEmail($member);
		return Redirect::action('MemberController@showApproveMember')->with('message', 
				$member->first_name . " " . $member->last_name . " is now an approved member.");
	}

	/**
	 * Handle GET of recently added page
	 */
	public function recentlyAdded()
	{
		$message = Session::get('message', '');
		$number = Preference::find(1)->num_new_members; // This number of recent members
		$members = Member::orderBy('id', 'DESC')->take($number)->get();
		return View::make('member.newest')->with('members', $members);
	}

	/**
	 * Function to send signup email to a prospective member
	 *
	 * @param Member $member        	
	 */
	private function sendSignupEmail($member)
	{
		$email = $member->member_email;
		$name = $member->first_name . " " . $member->last_name;
		$username = $member->username;
		$password = $member->password;
		
		// Details for Mail delivery
		$user = array('email' => $email,'name' => $name
		);
		
		// Data to insert into email template
		$data = array('name' => $name,'username' => $username,'password' => $password
		);
		
		// Queue with Mail library
		Mail::queue('emails.signup', $data, 
				function ($message) use($user)
				{
					$message->to($user['email'], $user['name'])->subject(
							'Thanks for signing up to the Cork Cloud Library!');
				});
	}

	/**
	 * Function to send email to approved member
	 *
	 * @param Member $member        	
	 */
	private function sendApprovalEmail($member)
	{
		$email = $member->member_email;
		$name = $member->first_name . " " . $member->last_name;
		$username = $member->username;
		$password = $member->password;
		
		// Details for Mail delivery
		$user = array('email' => $email,'name' => $name
		);
		
		// Data to insert into email template
		$data = array('name' => $name,'username' => $username,'password' => $password
		);
		
		// Queue with Mail library
		Mail::queue('emails.approved', $data, 
				function ($message) use($user)
				{
					$message->to($user['email'], $user['name'])->subject('Welcome to the Cork Cloud Library!');
				});
	}
}
