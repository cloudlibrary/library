<?php

class SecurityController extends BaseController
{

	public function showLogin()
	{
		return View::make('login');
	}

	public function doLogin()
	{
		$userdata = array('username' => Input::get('username'),'password' => Input::get('password')
		);
		
		if (Auth::attempt($userdata))
		{
			$memberId = Auth::user()->member_id;
			if ($memberId == 0)
			{
				// Librarian or admin
				return Redirect::to('home');
			}
			else
			{
				$member = Member::find($memberId);
				if ($member->approved)
				{
					// Member has been approved
					return Redirect::to('home');
				}
				else
				{
					// Member has given correct details but is not yet approved
					Auth::logout();
					return Redirect::to('login')->withErrors("You must wait for approval before being able to log in.");
				}
			}
		}
		else
		{
			return Redirect::to('login')->withErrors("Could not log you into the system.");
		}
	}

	public function doLogout()
	{
		Auth::logout();
		return Redirect::to('login');
	}
}