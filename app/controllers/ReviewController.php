<?php

class ReviewController extends \BaseController
{

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::all();
		$validator = Review::validate($inputs);
		
		if ($validator->passes())
		{
			$previousReviews = Review::where('member_id', Auth::user()->member_id)->where('book_id', $inputs['book_id'])->get();
			if (count($previousReviews) > 0)
			{
				// The user has made a review already
				return Redirect::route('book.show', $inputs['book_id'])->with('message', 
						'You have already reviewed this book!');
			}
			
			$review = new Review();
			$review->member_id = Auth::user()->member_id;
			$review->book_id = $inputs['book_id'];
			$review->review_score = $inputs['review_score'];
			$review->comment = $inputs['comment'];
			$review->save();
			
			return Redirect::route('book.show', $inputs['book_id'])->with('message', 
					'Your review has been added successfuly!');
		}
		else
		{
			return Redirect::route('book.show', $inputs['book_id'])->withErrors($validator);
		}
	}
}