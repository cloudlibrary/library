<?php

class ContactusController extends BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('contactus');
	}

	public function postContactform()
	{
		$this->beforeFilter('csrf', array('on' => 'post'
		));
		$rules = array('name' => 'required','subject' => 'required','message' => 'required|min:20',
				'sender' => 'required|email'
		);
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes())
		{
			// Send an email to our account based on user input
			$inputs = array('name' => Input::get('name'),'sender' => Input::get('sender'),
					'subject' => Input::get('subject'),'message' => Input::get('message')
			);
			$this->sendContactEmail($inputs);
			
			return Redirect::to('contactus')->with('message', 'Thank You , Your message has been sent !');
		}
		else
		{
			return Redirect::to('contactus')->withErrors($validator);
		}
	}

	private function sendContactEmail($inputs)
	{
		// Data for 'from' fields
		$user = array('email' => $inputs['sender'],'name' => $inputs['name']
		);
		
		// Input data to insert into email body
		$data = array('contactName' => $inputs['name'],'contactSender' => $inputs['sender'],
				'contactSubject' => $inputs['subject'],'contactMessage' => $inputs['message']
		);
		
		// Use Mail queue
		Mail::queue('emails.contact', $data, 
				function ($message) use($user)
				{
					$message->from($user['email'], $user['name']);
					$message->to('corkcloudlibrary@gmail.com', 'Cloud Library')->subject(
							'Message Sent Via Contact Form');
				});
	}

	public function getLang($lang = 'en')
	{
		Session::put('lang', $lang);
		return Redirect::back();
	}
}