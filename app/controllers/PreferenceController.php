<?php

class PreferenceController extends \BaseController
{

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function edit($id)
	{
		// Preference table has one entry that holds all administrative
		// preferences like loan period, fine amount, etc.
		$preference = Preference::find($id);
		return View::make('preference.edit')->with('preference', $preference);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function update($id)
	{
		$inputs = Input::all();
		$validator = Preference::validate($inputs);
		
		if ($validator->passes())
		{
			$preference = preference::find($id);
			
			if ($inputs['initial_book_allowance'] >= $inputs['oneyear_book_allowance'])
			{
				return Redirect::route('preference.edit', $id)->with('message', 
						'One year allowance must be greater than initial allowance.');
			}
			
			$preference->initial_book_allowance = $inputs['initial_book_allowance'];
			$preference->oneyear_book_allowance = $inputs['oneyear_book_allowance'];
			$preference->loan_period = $inputs['loan_period'];
			$preference->fine_amount = $inputs['fine_amount'];
			$preference->num_highest_rated = $inputs['num_highest_rated'];
			$preference->num_most_borrowed = $inputs['num_most_borrowed'];
			$preference->num_new_members = $inputs['num_new_members'];
			$preference->update();
			
			// Prompt use of routine that checks members with membership past one year
			$this->updateExistingMembers($inputs['initial_book_allowance'], $inputs['oneyear_book_allowance']);
			
			return Redirect::route('preference.edit', $id)->with('message', 'Preferences updated.');
		}
		else
		{
			return Redirect::route('preference.edit', $id)->withErrors($validator);
		}
	}

	private function updateExistingMembers($initialAllowance, $oneYearAllowance)
	{
		$members = Member::all();
		
		// Check all members
		foreach($members as $member)
		{
			// Get the join date of each member
			$joinDate = strtotime($member->created_at);
			$rawDays = (time() - $joinDate) / (60 * 60 * 24);
			$days = round($rawDays);
			if ($days > 365 && $member->fine_balance == 0)
			{
				// If over a year and no fines
				$member->member_allowance = $oneYearAllowance;
			}
			else
			{
				// Just update with new value
				$member->member_allowance = $initialAllowance;
			}
			$member->update();
		}
	}
}