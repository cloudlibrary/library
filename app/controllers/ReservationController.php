<?php

class ReservationController extends \BaseController
{

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('reservation.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::all();
		$validator = Reservation::validate($inputs);
		
		if ($validator->passes())
		{
			$book_id = $inputs['book_id'];
			$existingReservations = Reservation::where('book_id', $book_id)->get();
			if (count($existingReservations) > 0)
			{
				$existingReservation = $existingReservations[0];
				$resTime = strtotime($existingReservation->created_at);
				$rawDays = (time() - $resTime) / (60 * 60 * 24);
				$days = round($rawDays);
				if ($days < 8)
				{
					// Can't double reserve
					return Redirect::action('ReservationController@create')->with('message', 
							'Sorry. This book is already reserved.');
				}
				
				// If we get to here, this is an old reservation, which we can delete
				$existingReservation->delete();
			}
			
			// Make new reservation
			$reservation = new Reservation();
			$reservation->member_id = Auth::user()->member_id;
			$reservation->book_id = $book_id;
			$reservation->save();
			
			$book = Book::find($book_id);
			$book->status = 'reserved';
			$book->update();
			
			return Redirect::action('ReservationController@create')->with('message', 
					'Congratulation, the book is reserved succesfully.');
		}
		else
		{
			return Redirect::route('reservation.create')->withErrors($validator);
		}
	}
}